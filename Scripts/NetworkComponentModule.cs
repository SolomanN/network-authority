using System;
#if MIRROR
using Mirror;
#endif
using UnityEngine;

namespace NetworkAuthority
{
    #if MIRROR
    public abstract class NetworkComponentModule : NetworkBehaviour
    {
        #if UNITY_EDITOR
        public bool hideComponents = true;
        #endif
        
        public abstract Type componentType { get; }

        [ContextMenu("Reset Script Visibility")]
        private void ResetVisibility()
        {
            foreach (Component c in GetComponents<Component>())
            {
                c.hideFlags &= ~HideFlags.HideInInspector;
            }
        }
    }
    #endif
}