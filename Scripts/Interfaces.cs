namespace NetworkAuthority
{
    // ReSharper disable once IdentifierTypo
    public interface INetworkInitCallbacks
    {
        /// <inheritdoc cref="Mirror.NetworkBehaviour.OnStartAuthority"/>
        void OnStartAuthority();
        /// <inheritdoc cref="Mirror.NetworkBehaviour.OnStartServer"/>
        void OnStartServer();
        /// <inheritdoc cref="Mirror.NetworkBehaviour.OnStartLocalPlayer"/>
        void OnStartLocalPlayer();
        /// <inheritdoc cref="Mirror.NetworkBehaviour.OnStartClient"/>
        void OnStartClient();
        /// <inheritdoc cref="Mirror.NetworkBehaviour.OnStopAuthority"/> 
        void OnStopAuthority();
    }

}