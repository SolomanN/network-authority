﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace NetworkAuthority.Editors
{
    public static class BuildHelper
    {
        public const string ClientBuildPath = "Builds/Client";
        public const string ServerBuildPath = "Builds/Server";
        
        [MenuItem("Tools/Builds/Win x64 Client (dev)")]
        public static void BuildDevClient()
        {
            EnsureDirectory(ClientBuildPath);
            BuildOptions options = BuildOptions.AutoRunPlayer | BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.StrictMode;
            Build(ClientBuildPath, "Client.exe", options);
        }

        [MenuItem("Tools/Builds/Win x64 Client")]
        public static void BuildClient()
        {
            EnsureDirectory(ClientBuildPath);
            BuildOptions options = BuildOptions.AutoRunPlayer | BuildOptions.StrictMode;
            Build(ClientBuildPath, "Client.exe", options);
        }

        [MenuItem("Tools/Builds/Win x64 Server (headless)")]
        public static void BuildServer()
        {
            EnsureDirectory(ServerBuildPath);
            BuildOptions options = BuildOptions.AutoRunPlayer | BuildOptions.StrictMode | BuildOptions.EnableHeadlessMode;
            Build(ServerBuildPath, "Server.exe", options);
        }

        private static void EnsureDirectory(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        private static void Build(string path, string exe, BuildOptions options)
        {
            BuildReport report = BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, Path.Combine(path, exe),
                BuildTarget.StandaloneWindows64, options);
            
            Debug.Log("Build finished with result: " + report.summary.result);
        }
    }
}
