﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEditorInternal;
using UnityEngine;

namespace NetworkAuthority.Editors
{
#if MIRROR
    [CustomEditor(typeof(NetworkComponentModule), true)]
    public class NetworkComponentModuleEditor : Editor
    {
        private NetworkComponentModule componentModule;
        private ReorderableList list;

        private GUIContent scriptIconContent;
        private Editor currentEditor;
        
        private void OnEnable()
        {
            componentModule = (NetworkComponentModule) target;
            scriptIconContent = EditorGUIUtility.IconContent("cs Script Icon");
            
            Component[] components = GetExisting();
            list = new ReorderableList(components, componentModule.componentType)
            {
                onReorderCallback = OnListReorder,
                onAddDropdownCallback = OnAddDropdown,
                onRemoveCallback = OnListRemove,
                drawHeaderCallback = OnDrawHeader,
                drawElementCallback = OnDrawElement
            };
            
            ToggleVisibility(!componentModule.hideComponents);
            ReOrder(list);
        }

        private void OnDrawElement(Rect rect, int index, bool isactive, bool isfocused)
        {
            GUIContent content = new GUIContent(scriptIconContent) 
                { text = list.list[index].GetType().Name };
            EditorGUI.LabelField(rect, content);
        }

        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                DrawPropertiesExcluding(serializedObject, nameof(NetworkComponentModule.hideComponents), "m_Script");

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField($"{componentModule.GetType().Name} Components", EditorStyles.toolbarButton);
                EditorGUILayout.Space();
                bool initialDraw = componentModule.hideComponents;
                componentModule.hideComponents = EditorGUILayout.Toggle("Hide Components", componentModule.hideComponents);
                
                list.DoLayoutList();

                if (componentModule.hideComponents && list.index != -1)
                {
                    Component c = list.list[list.index] as Component;
                    CreateCachedEditor(c, null, ref currentEditor);
                    if (currentEditor != null)
                    {
                        currentEditor.OnInspectorGUI();
                    }
                }
                
                if (check.changed)
                {
                    serializedObject.ApplyModifiedProperties();

                    if (initialDraw != componentModule.hideComponents)
                    {
                        ToggleVisibility(!componentModule.hideComponents);
                    }
                }
                
                EditorGUILayout.EndVertical();
            }
        }

        private void OnDrawHeader(Rect rect)
        {
            EditorGUI.LabelField(rect, "Modules");
        }

        private void OnListRemove(ReorderableList l)
        {
            int index = l.index;
            var component = (Component)l.list[index];
            if (component == null) return;
            DestroyImmediate(component, true);
            OnEnable();
        }

        private void OnAddDropdown(Rect buttonRect, ReorderableList l)
        {
            GenericMenu dropdownMenu = new GenericMenu();
            
            IEnumerable<Type> foundTypes = FindTypes();
            foreach (Type t in foundTypes)
            {
                GUIContent typeName = new GUIContent(t.FullName);
                dropdownMenu.AddItem(typeName, false, () => componentModule.gameObject.AddComponent(t));
            }
            
            dropdownMenu.ShowAsContext();
        }

        private IEnumerable<Type> FindTypes()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<Type> foundTypes = new List<Type>();
            foreach (Assembly ass in assemblies)
            {
                Type[] types = ass.GetTypes();
                foreach (Type type in types)
                {
                    if (type.IsAbstract) continue;
                    if (!componentModule.componentType.IsAssignableFrom(type)) continue;
                    foundTypes.Add(type);
                }
            }
            return foundTypes.ToArray();
        }

        private void OnListReorder(ReorderableList l)
        {
            ReOrder(l);
        }

        private void ReOrder(ReorderableList l)
        {
            Component[] allComponents = componentModule.gameObject.GetComponents<Component>();
            int targetIndex = Array.IndexOf(allComponents, componentModule);
            if (targetIndex == -1)
                return;

            for (var i = 0; i < l.list.Count; i++)
            {
                Component element = l.list[i] as Component;
                if (SetComponentIndex(allComponents, element, targetIndex + 1 + i))
                {
                    allComponents = componentModule.gameObject.GetComponents<Component>();
                }
            }
        }

        private Component[] GetExisting()
        {
            return componentModule.GetComponents(componentModule.componentType);
        }

        private void ToggleVisibility(bool visible)
        {
            if (componentModule != null)
            {
                Component[] comps = GetExisting();
                foreach (Component c in comps)
                {
                    if (visible)
                    {
                        c.hideFlags &= ~HideFlags.HideInInspector;
                    }
                    else
                    {
                        c.hideFlags |= HideFlags.HideInInspector;
                    }
                }

                if (EditorSceneManager.IsPreviewScene(componentModule.gameObject.scene))
                {
                    EditorSceneManager.MarkSceneDirty(componentModule.gameObject.scene);
                }
            }
        }

        private static bool SetComponentIndex(Component[] allComponents, Component target, int targetIndex)
        {
            if (allComponents == null)
                allComponents = target.gameObject.GetComponents<Component>();

            if (allComponents.Length <= 0)
                return false;

            if (targetIndex < 0 || targetIndex >= allComponents.Length)
                return false;

            int index = Array.IndexOf(allComponents, target);
            if (index == -1)
                return false;

            if (index == targetIndex)
                return true;

            while (index != targetIndex)
            {
                if (index > targetIndex)
                {
                    if (!ComponentUtility.MoveComponentUp(target))
                        return false;
                    index--;
                }
                else if (index < targetIndex)
                {
                    if (!ComponentUtility.MoveComponentDown(target))
                        return false;
                    index++;
                }
            }

            return true;
        }
    }
#endif
}
