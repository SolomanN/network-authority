using System;

namespace NetworkAuthority
{
#if MIRROR
    /// <summary>
    /// A helper class that will call initialization functions on monobehaviours that are not able to
    /// implement networkbehaviour due to the limitations of unity's network weaver
    /// </summary>
    public class NetworkInitializer : NetworkComponentModule
    {
        enum InitializationType
        {
            StartClient,
            StartAuthority,
            StartServer,
            StartLocalPlayer
        }

        public override Type componentType => typeof(INetworkInitCallbacks);

        private INetworkInitCallbacks[] callbackBehaviours;

        private void Awake()
        {
            callbackBehaviours = GetComponentsInChildren<INetworkInitCallbacks>();
        }

        public override void OnStartLocalPlayer()
        {
            InitialiseBehaviours(InitializationType.StartLocalPlayer);
        }

        public override void OnStartClient()
        {
            InitialiseBehaviours(InitializationType.StartClient);
        }

        public override void OnStartAuthority()
        {
            if (isServerOnly)
                return;

            InitialiseBehaviours(InitializationType.StartAuthority);
        }

        public override void OnStartServer()
        {
            if (isClientOnly)
                return;

            InitialiseBehaviours(InitializationType.StartServer);
        }

        private void InitialiseBehaviours(InitializationType type)
        {
            foreach (INetworkInitCallbacks beh in callbackBehaviours)
            {
                switch (type)
                {
                    case InitializationType.StartAuthority:
                        beh.OnStartAuthority();
                        break;
                    case InitializationType.StartClient:
                        beh.OnStartClient();
                        break;
                    case InitializationType.StartLocalPlayer:
                        beh.OnStartLocalPlayer();
                        break;
                    case InitializationType.StartServer:
                        beh.OnStartServer();
                        break;
                }
            }
        }
    }
#endif
}